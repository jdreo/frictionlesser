#!/bin/sh

seedbase="$1"
runs="$2"

# mkdir -p logs

FRICTIONLESSER="../../release/app/frictionlesser"

size=20
mkdir -p data/output/signature_of_${size}-genes
mkdir -p data/inter/trajs/
mkdir -p data/inter/logs/

for r in $(seq $runs); do
    seed=$((seedbase+r))
    echo "Submission $r: z${size}_s${seed}"
    # Explicit sysbion and long partition, but *no QoS*, to avoid applying it to sysbio as well.
    cmd="sbatch --job-name=z${size}_s${seed} --mem=6000 --partition=long,sysbio -e data/inter/logs/slurm_z${size}_s${seed}_job%j.err -o data/inter/logs/slurm_z${size}_s${seed}_job%j.out ./submit-run.sh ${size} ${seed}"
    echo "$cmd"
    $cmd
done

