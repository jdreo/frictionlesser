#!/bin/bash

module load Python/3.8.1 # For snakemake
module load snakemake/7.16.1
# module load Python/3.11.5 # For scanpy

cd frictionlesser/scripts/paris/

snakemake --cores 1 --snakefile preproc.Snakefile

